'use strict';

const path = require('path');

const execute = require('./execute');

function build (dir) {
	return execute(dir, ['node', './node_modules/fontoxml-build/bin/fontoxml-build']);
}

module.exports = fotno => {
	fotno.registerCommand('build')
		.setDescription(`Use the official FontoXML tools to build the application. Runs the equivalent of "npm run build"`)
		.setController((req, res) => {
			res.caption(`fotno build`);

			const code = req.fdt.editorRepository;

			res.properties({
				'Application': code.name,
				'SDK version': code.sdkVersion,
				'Path': code.path
			});


			res.caption('Build output');

			const emitter = build();

			emitter.on('log', data => res.debug(('' + data).trim()));

			emitter.on('error', data => {
				res.break();
				res.error(('' + data).trim());
			});

			emitter.on('close', function () {
				res.break();
				res.success('End of build task');
				res.break();
			})
		});
};
