'use strict';

const EventEmitter = require('events').EventEmitter;
const spawn = require('child_process').spawn;

module.exports = function execute(cwd, argv) {
	const eventEmitter = new EventEmitter(),
		spawnedProcess = spawn(argv[0], argv.splice(1), {
			cwd: cwd
		});

	spawnedProcess.stdout.on('data', (data) => {
		eventEmitter.emit('log', data);
	});

	spawnedProcess.stderr.on('data', (data) => {
		eventEmitter.emit('error', data);
	});

	spawnedProcess.on('close', (code) => {
		eventEmitter.emit('close', code);
		eventEmitter.removeAllListeners();
	});

	return eventEmitter;
};
