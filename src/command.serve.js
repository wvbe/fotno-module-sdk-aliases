'use strict';

const path = require('path');
const fs = require('fs');
const os = require('os');

const execute = require('./execute');

function serve (dir, options) {
	const concatenatedConfig = {
			root: dir,
			savemode: options.save,
			port: options.port,
			documentLoadLock: options.lock,
			delay: options.delay,
			dist: !!options.dist,
			verbose: !!options.verbose,
			open: !!options.open
		};

	let args = ['node', './node_modules/fontoxml-dev-server/bin/fontoxml-dev-server'];

	args = args.concat(['--port', concatenatedConfig.port]);

	if(concatenatedConfig.dist)
		args = args.concat(['--dist']);

	if(concatenatedConfig.open)
		args = args.concat(['--open']);

	if(concatenatedConfig.delay)
		args = args.concat(['--delay', concatenatedConfig.delay]);

	if(concatenatedConfig.savemode)
		args = args.concat(['--savemode', concatenatedConfig.savemode]);

	if(concatenatedConfig.documentLoadLock && !concatenatedConfig.documentLoadLock.isLockAvailable)
		args = args.concat(['--lock-not-available']);

	if(concatenatedConfig.documentLoadLock && !concatenatedConfig.documentLoadLock.isLockAcquired)
		args = args.concat(['--lock-not-acquired']);

	if(concatenatedConfig.verbose)
		args = args.concat(['--verbose']);

	return execute(concatenatedConfig.root, args);
}
module.exports = fotno => {
	function serveApplicationController (req, res) {
		res.caption(`fotno serve`);

		const code = req.fdt.editorRepository;

		res.properties({
			'Application': code.name,
			'SDK version': code.sdkVersion,
			'Path': code.path
		});

		if(req.options.dist) {
			res.caption('Credentials');
			try {
				res.properties(fs.readFileSync(path.join(code.path, 'users.htpasswd'), 'utf-8')
					.split(os.EOL)
					.filter(line => !!line.trim())
					.map(line => line.split(':'))
				);
			} catch (e) {
				res.error(e);
			}
		}

		res.caption('Server output');

		const emitter = serve(code.path, req.options);

		emitter.on('log', data => res.debug(('' + data).trim()));

		emitter.on('error', data => {
			res.break();
			res.error(('' + data).trim());
		});

		emitter.on('close', function () {
			res.break();
			res.success('End of server process');
			res.break();
		})
	}

	const SAVE_MODES = ['disk', 'session', 'off'];

	fotno.registerCommand('serve')
		.setDescription(`Use the official FontoXML tools to serve the application. Runs the equivalent of "npm run server", but has different defaults and a more lenient syntax.`)
		.setLongDescription(`This command uses the version of fontoxml-dev-server and fontoxml-build-tools packaged with your FontoXML application.`)
		.addExample(`fotno serve`, `Start the development server on port 8080, without savemode`)
		.addExample(`fotno serve -ovsp 9999 --dist`, `Start the build server on port 9999 with savemode=disk and open in the browser right away.`)
		.setController(serveApplicationController)
		.addOption('open', 'o', 'Open a new browser tab for eachserved application', false)
		.addOption('dist', 'P', 'Serve from dist/ folder', false)
		.addOption('verbose', 'v', 'Verbose, not sure what that means tho', false)
		.addOption(new fotno.Option('port')
			.setShort('p')
			.setDescription('Port number to serve the application on, defaults to 8080')
			.setDefault(8080, true)
			.setResolver(parseInt)
		)
		.addOption(new fotno.Option('delay')
			.setShort('d')
			.setDescription('Delay in milliseconds for CMS routes, defaults to 0 if omitted or 3000 if left empty.')
			.setDefault(3000, false)
			.setResolver(val => {
				val = parseInt(val);
				return (!val || isNaN(val)) ? 0 : val;
			})
		)
		.addOption(new fotno.Option('save')
			.setShort('s')
			.setDescription('Save mode, defaults to "off" if omitted or "disk" if empty. Must be one of ' + SAVE_MODES.join('|') + '.')
			.setDefault('disk', false)
			.setResolver(val => {
				if(!val)
					val = 'off';

				if(!SAVE_MODES.includes(val))
					throw new fotno.InputError(`"${val}" is not a valid save mode, must be one of ${SAVE_MODES.join('|')}.`)

				return val;
			})
		)
		.addOption(new fotno.Option('lock')
			.setShort('l')
			.setDescription('Lock status, defaults to "ok" if omitted or "na" if left empty. Must be one of ok|nav|naq|na.')
			.setDefault('na', false) // this is the default value if the -l flag is used but empty
			.setResolver(val => {
				switch (val) {
					case undefined: // this is the default value if the -l flag is *not* used
					case '1':
					case 'true':
					case 'ok':
						return { isLockAvailable: true, isLockAcquired: true };
					case 'nav':
						return { isLockAvailable: false, isLockAcquired: true };
					case 'naq':
						return { isLockAvailable: true, isLockAcquired: false };
					case '0':
					case 'false':
					case 'na':
						return { isLockAvailable: false, isLockAcquired: false };
					default:
						throw new fotno.InputError(
							`"${val}" is not a valid lock option, must be one of ok|nav|naq|na.`,
							`Must be either "ok", "nav", "naq" or "na". To help you remember, "nav" = Not islockAVailable, "naq" = Not islockAcQuired, "na" = Not Anything`
						);
				}
			})
		);
};
