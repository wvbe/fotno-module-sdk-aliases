module.exports = fotno => {
	[
		require('./src/command.serve.js'),
		require('./src/command.build.js')
	].forEach(mod => mod(fotno));
};
